name = 'Zed A. Shaw'
age = 35 # a lie
height = 74 # is it?
weight = 180 # maybe
eyes = 'blue'
teeth = 'white'
hair = 'missing'

print "let's talk about this %s." % name
print "he's %d inches tall." % height
print "he weighs %d pounds" % weight
print "so, not too heavy then..."
print "he's got %s eyes and %s hair." % (eyes, hair)
print "his teeth are usually %s (coffee dependent)" % teeth

# this line is tricky etc.
print "if I add %d, %d, and %d I get %d" % (age, height, weight, age + height + weight)

