from sys import argv
from os.path import exists

script, from_f, to_f = argv

print "copying from %s to %s" % (from_f, to_f)

# in_file = open(from_f)
in_data = open(from_f).read()

print "%s is %d bytes long" % (from_f, len(in_data))

print "Does the output file exist? %r" % exists(to_f)
print "READY. Hit RETURN to continue or ^C to exit."
raw_input()

out_file = open(to_f, 'w')
out_file.write(in_data)

print "Done."

out_file.close()
# in_file.close()

